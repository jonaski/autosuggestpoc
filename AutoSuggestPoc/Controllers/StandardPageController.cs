﻿using System.Web.Mvc;
using AutoSuggestPoc.Models.Pages;
using EPiServer.Web.Mvc;

namespace AutoSuggestPoc.Controllers
{
    public class StandardPageController : PageController<StandardPage>
    {
        public ActionResult Index(StandardPage currentPage)
        {
            return View(currentPage);
        }
    }
}