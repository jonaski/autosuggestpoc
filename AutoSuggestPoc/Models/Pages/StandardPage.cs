﻿using System.ComponentModel.DataAnnotations;
using AutoSuggestPoc.Infrastructure;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;

namespace AutoSuggestPoc.Models.Pages
{
    [ContentType(DisplayName = "Standard page", GUID = "961f4884-6071-41f7-a755-309052ac1682", Description = "")]
    public class StandardPage : PageData
    {
        [AutoSuggestSelection(typeof(GroupSelectionQuery))]
        [Display(Name = "Lookup value")]
        public virtual string LookupValue { get; set; }
    }
}