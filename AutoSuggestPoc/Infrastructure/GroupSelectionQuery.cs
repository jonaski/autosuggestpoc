﻿using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using System.Collections.Generic;
using System.Linq;

namespace AutoSuggestPoc.Infrastructure
{
    [ServiceConfiguration(typeof(ISelectionQuery))]
    public class GroupSelectionQuery : ISelectionQuery
    {
        private readonly ICollection<string> _groups;

        public GroupSelectionQuery()
        {
            _groups = new List<string>
            {
                @"DOMAIN\GroupOne",
                @"DOMAIN\GroupTwo",
                @"DOMAIN\GroupThree",
                @"DOMAIN\GroupFour"
            };
        }

        public ISelectItem GetItemByValue(string value)
        {
            string group = _groups.SingleOrDefault(g => g == value);

            return new SelectItem
            {
                Text = group,
                Value = group
            };
        }

        public IEnumerable<ISelectItem> GetItems(string query)
        {
            return _groups.Where(g => g.StartsWith(query)).Select(group => new SelectItem
            {
                Text = group,
                Value = group
            });
        }
    }
}